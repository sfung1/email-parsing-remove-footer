﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Text.RegularExpressions;
using kCura.Agent;
using DTOs = kCura.Relativity.Client.DTOs;
using kCura.Relativity.Client;
using Relativity.API;
using System.Data.SqlClient;

namespace EmailParsingAgent
{
    [kCura.Agent.CustomAttributes.Name("Email Parsing Agent")]
    [System.Runtime.InteropServices.Guid("DE347FC2-02B1-4B1D-939F-E8E12BF45FD2")]

    public class EmailParsingAgent : kCura.Agent.AgentBase
    {
        public static readonly Guid IDENTIFIEDFOOTER_RDO_GUID = new Guid("C8FA1E8C-DB31-4D26-BDFB-0BAF5B299B52");
        public static readonly Guid EMAIL_FIELD_GUID = new Guid("63639991-EFF8-4D66-BC4A-B9ED1BE4F7A5");
        
                
        public override void Execute()
        {
            EmailParsingJobDTO documentsToProcessJobDTO = null;
            IDBContext eddsDBContext = this.Helper.GetDBContext(-1);
            Int32 agentArtifactID = this.AgentID;

            try
            {
                this.RaiseMessage("Looking for the next job in the queue", 10);
                documentsToProcessJobDTO = GetNextEmailParsingJob(eddsDBContext, agentArtifactID);
                

                while (documentsToProcessJobDTO != null)
                {
                    this.RaiseMessage(string.Format("Working on Workspace: {0} for Document : {1}", documentsToProcessJobDTO.WorkspaceArtifactID, documentsToProcessJobDTO.DocumentArtifactID),10);
                    IDBContext caseDBContext = this.Helper.GetDBContext(documentsToProcessJobDTO.WorkspaceArtifactID);

                    DataTable docEmail = GetQueue();

                    string emailAuthorField = getEmailAuthorField(caseDBContext);


                    string email = null;
                    
                    SqlParameter documentArtifactIDParam = new SqlParameter("@documentArtifactID", System.Data.SqlDbType.Int);
                    SqlParameter workspaceArtifactIDParam = new SqlParameter("@workspaceArtifactID", System.Data.SqlDbType.Int);

                    documentArtifactIDParam.Value = documentsToProcessJobDTO.DocumentArtifactID;
                    workspaceArtifactIDParam.Value = documentsToProcessJobDTO.WorkspaceArtifactID;

                    

                    string getProcessedEmailsSQL = @"SELECT  ArtifactID,
                                                            [Name]
                                                FROM    [EDDSDBO].[IdentifiedFooters] [if] (NOLOCK)";

                    
                    DataTable processedEmails = caseDBContext.ExecuteSqlStatementAsDataTable(getProcessedEmailsSQL);

                   
                    foreach (DataRow drow in docEmail.Rows)
                    {
                        email = drow[emailAuthorField].ToString();
                        Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
                        MatchCollection emailMatches = emailRegex.Matches(email);
                        #region check if there is an e-mail address parsed
                        if (emailMatches.Count > 0)
                        {
                            
                            foreach (Match m in emailMatches)
                            {
                                drow[emailAuthorField] = m.Value;

                                int existingEmail = (from DataRow dr in processedEmails.Rows
                                                     where (string)dr["Name"] == drow[emailAuthorField].ToString()
                                                     select (int)dr["ArtifactID"]).FirstOrDefault();

                                using (IRSAPIClient proxy = new RSAPIClient(new Uri("https://reltest-data-01.advanced.local/Relativity.services"), new IntegratedAuthCredentials()))
                                {
                                    proxy.APIOptions.WorkspaceID = documentsToProcessJobDTO.WorkspaceArtifactID;

                                    #region check ifemail address created
                                    //check to see if the e-mail address has already been created
                                    if (existingEmail > 0)
                                    {
                                        //check to see if the document has been processed in the past
                                        DataTable docCheckdt = DocumentProcessCheck(documentsToProcessJobDTO.WorkspaceArtifactID, documentsToProcessJobDTO.DocumentArtifactID);

                                        int docExists = int.Parse((from d in docCheckdt.AsEnumerable()
                                                                   select d.Field<int>("COUNT")).FirstOrDefault().ToString());

                                        if (docExists == 0)
                                        {
                                            //link only the document
                                            LinkDocument(documentsToProcessJobDTO.WorkspaceArtifactID, documentsToProcessJobDTO.DocumentArtifactID, existingEmail);
                                        }

                                    }
                                    else
                                    {
                                        //create the RDO, link the document to the RDO
                                        //step 1 look for rdo
                                        var dto = new DTOs.RDO();
                                        dto.ArtifactTypeGuids.Add(new Guid("C8FA1E8C-DB31-4D26-BDFB-0BAF5B299B52"));

                                        //step 2 add email to the names field
                                        dto.Fields.Add(new DTOs.FieldValue(new Guid("A92CBB40-8B94-4AC3-9D87-F3AFD1C86E9F"), drow[emailAuthorField]));

                                        DTOs.FieldValueList<DTOs.Document> objects = new DTOs.FieldValueList<DTOs.Document>();
                                        //add document
                                        objects.Add(new DTOs.Document(documentsToProcessJobDTO.DocumentArtifactID));

                                        //add document to this domains field
                                        dto.Fields.Add(new DTOs.FieldValue(new Guid("63639991-EFF8-4D66-BC4A-B9ED1BE4F7A5"), objects));

                                        //step 3 use create()
                                        DTOs.WriteResultSet<DTOs.RDO> writeResults = proxy.Repositories.RDO.Create(dto);
                                    }

                                    #endregion
                                }


                            }

                            string updateCompleteQueueSQL = @"UPDATE  [dbo].[EmailParsingQueue]
                                        SET     [Status] = 2
                                        WHERE   [DocumentArtifactID] = @documentArtifactID AND
                                                [WorkspaceArtifactID] = @workspaceArtifactID";

                            eddsDBContext.ExecuteNonQuerySQLStatement(updateCompleteQueueSQL, new SqlParameter[] { documentArtifactIDParam, workspaceArtifactIDParam });
                        }
                        else
                        {
                            string updateCompleteQueueSQL = @"UPDATE  [dbo].[EmailParsingQueue]
                                        SET     [Status] = 2
                                        WHERE   [DocumentArtifactID] = @documentArtifactID AND
                                                [WorkspaceArtifactID] = @workspaceArtifactID";

                            eddsDBContext.ExecuteNonQuerySQLStatement(updateCompleteQueueSQL, new SqlParameter[] { documentArtifactIDParam, workspaceArtifactIDParam });

                        }
                        #endregion
                    }



                }


                
            }
            catch (System.Exception ex)
            {
                RaiseError("Error" + ex.ToString(),ex.ToString());
            }


        }
 
        public DataTable GetQueue()
        {
            //get first document in queue assigned to this agent
            string emailParsingQueueSQL = @"SELECT TOP 1 [WorkspaceArtifactID],
                                                [DocumentArtifactID]
                                        FROM    [dbo].[EmailParsingQueue] epq
                                        WHERE   [AgentArtifactID] = @agentArtifactID AND
                                                [Status] = 1";

            SqlParameter agentArtifactIDParam = new SqlParameter("@agentArtifactID", System.Data.SqlDbType.Int);
            agentArtifactIDParam.Value = this.AgentID;

            //retrieves the queue
            IDBContext eddsDBContext = Helper.GetDBContext(-1);
            DataTable workThisDoc = eddsDBContext.ExecuteSqlStatementAsDataTable(emailParsingQueueSQL, new SqlParameter[] { agentArtifactIDParam });

            int workSpaceArtifactID = int.Parse((from m in workThisDoc.AsEnumerable()
                                                 select m.Field<int>("WorkspaceArtifactID")).FirstOrDefault().ToString());
            
            int documentArtifactID = int.Parse((from m in workThisDoc.AsEnumerable()
                                                select m.Field<int>("DocumentArtifactID")).FirstOrDefault().ToString());

            string getDocumentInfoSQL = @"SELECT  [ArtifactID],
                                                [Author]
                                        FROM    [EDDSDBO].[Document] d ( NOLOCK )
                                        WHERE   [ArtifactID] = @documentArtifactID";

            SqlParameter documentArtifactIDParam = new SqlParameter("@documentArtifactID", System.Data.SqlDbType.Int);
            documentArtifactIDParam.Value = documentArtifactID;

            IDBContext runCase = Helper.GetDBContext(workSpaceArtifactID);
            DataTable docList = runCase.ExecuteSqlStatementAsDataTable(getDocumentInfoSQL, new SqlParameter[] { documentArtifactIDParam });

            return docList;
        }

        public DataTable DocumentProcessCheck(int workspaceartifactid, int documentartifactid)
        {

            string tableFieldNames = @"SELECT  [TableName] = '[eddsdbo].[' +
                                    ( SELECT    [FieldName] = 'f' + CAST([ArtifactID] AS NVARCHAR(10))
                                      FROM      [EDDSDBO].[Field] f
                                      WHERE     [FieldArtifactTypeID] = ( SELECT    [DescriptorArtifactTypeID]
                                                                          FROM      [EDDSDBO].[ObjectType] ot
                                                                          WHERE     [Name] = 'IdentifiedFooters'
                                                                        ) AND
                                                [FieldTypeID] = 13
                                    ) +
                                    ( SELECT    [FieldName] = 'f' + CAST([ArtifactID] AS NVARCHAR(10))
                                      FROM      [EDDSDBO].[Field] f
                                      WHERE     [AssociativeArtifactTypeID] = ( SELECT  [DescriptorArtifactTypeID]
                                                                                FROM    [EDDSDBO].[ObjectType] ot
                                                                                WHERE   [Name] = 'IdentifiedFooters'
                                                                              )
                                    ) + ']',
                                    [DocumentFieldName] = ( SELECT  [FieldName] = 'f' +
                                                                    CAST([ArtifactID] AS NVARCHAR(10)) + 'ArtifactID'
                                                            FROM    [EDDSDBO].[Field] f
                                                            WHERE   [AssociativeArtifactTypeID] = ( SELECT
                                                                                          [DescriptorArtifactTypeID]
                                                                                          FROM
                                                                                          [EDDSDBO].[ObjectType] ot
                                                                                          WHERE
                                                                                          [Name] = 'IdentifiedFooters'
                                                                                          )
                                                          )";

            //gets db context of workspace
            IDBContext runCase = Helper.GetDBContext(workspaceartifactid);
            //retrieves a dt with the table name and fields of the multi object field
            DataTable docCheckTable = runCase.ExecuteSqlStatementAsDataTable(tableFieldNames);

            
            string tableName = (from t in docCheckTable.AsEnumerable()
                                    select t.Field<string>("TableName")).FirstOrDefault().ToString();
            string docFieldName = (from d in docCheckTable.AsEnumerable()
                                       select d.Field<string>("DocumentFieldName")).FirstOrDefault().ToString();

            //runs the most ghetto sql text string in the world - NOTE TO SELF: FIX THIS IF YOU CANT USE THE SERVICE API
            string docCheckSQL = @"SELECT [COUNT] = COUNT(*) FROM " + tableName + " WHERE " + docFieldName + " = " + documentartifactid.ToString();

            DataTable docCheck = runCase.ExecuteSqlStatementAsDataTable(docCheckSQL);

            return docCheck;
        }

        public void LinkDocument(int workspaceartifactid, int documentartifactid, int existingEmail)
        {

            ////use the DTOs to do this in the future! 
            //var artifact = new DTOs.RDO(existingEmail);
            //artifact.ArtifactTypeGuids.Add(IDENTIFIEDFOOTER_RDO_GUID);

            //var obj = new DTOs.Document(documentsToProcessJobDTO.DocumentArtifactID);


            ////add document to this domains field
            //artifact.Fields.Add(new DTOs.FieldValue(new Guid("63639991-EFF8-4D66-BC4A-B9ED1BE4F7A5"), obj));

            //proxy.Repositories.RDO.UpdateSingle(artifact);


            

            string emailArtifactTableSql = @"SELECT [TableName] = '[eddsdbo].[' + (SELECT  [FieldName] = 'f' + CAST([ArtifactID] AS NVARCHAR(10))
                                                                FROM    [EDDSDBO].[Field] f
                                                                WHERE   [FieldArtifactTypeID] = ( SELECT    [DescriptorArtifactTypeID]
                                                                                                  FROM      [EDDSDBO].[ObjectType] ot
                                                                                                  WHERE     [Name] = 'IdentifiedFooters'
                                                                                                ) AND
                                                                        [FieldTypeID] = 13) +
        
        
                                                                (SELECT  [FieldName] = 'f' + CAST([ArtifactID] AS NVARCHAR(10))
                                                                FROM    [EDDSDBO].[Field] f
                                                                WHERE   [AssociativeArtifactTypeID] = ( SELECT  [DescriptorArtifactTypeID]
                                                                                                        FROM    [EDDSDBO].[ObjectType] ot
                                                                                                        WHERE   [Name] = 'IdentifiedFooters'
                                                                                                      )) + ']'";

            IDBContext caseDBContext = Helper.GetDBContext(workspaceartifactid);
            DataTable emailArtifactTable = caseDBContext.ExecuteSqlStatementAsDataTable(emailArtifactTableSql);

            string emailTable = (from e in emailArtifactTable.AsEnumerable()
                                 select e.Field<string>("TableName")).FirstOrDefault().ToString();

            string insertLinkSQL = @"INSERT INTO " + emailTable + " SELECT " + existingEmail + "," + documentartifactid.ToString();
            caseDBContext.ExecuteNonQuerySQLStatement(insertLinkSQL);

        }
 
        public static EmailParsingJobDTO GetNextEmailParsingJob(IDBContext eddsDBContext, Int32 agentArtifactID)
        {
            //assign first available document in the queue to this agent
            EmailParsingJobDTO retVal = null;

            string nextJobSQL = @"DECLARE @nextJobID INT
                                BEGIN TRAN Tran1
                                SET @nextJobID = ( SELECT TOP 1
                                                            [ID]
                                                   FROM     [dbo].[EmailParsingQueue] WITH ( UPDLOCK, READPAST )
                                                   WHERE    [AgentArtifactID] = @agentArtifactID AND
                                                            [Status] = 1
                                                 )
                 
                                                 IF (@nextJobID IS NULL)
                                                 BEGIN
                                                 SET @nextJobID = (SELECT TOP 1 [ID] FROM [dbo].[EmailParsingQueue] WITH (UPDLOCK, READPAST)
                                                 WHERE [AgentArtifactID] = 0 AND [Status] = 0 ORDER BY [ID] ASC)
                 
                                                 UPDATE [dbo].[EmailParsingQueue]
                                                 SET [AgentArtifactID] = @agentArtifactID, [Status] = 1
                                                 WHERE [ID] = @nextJobID
                                                 END
                                COMMIT TRAN

                                SELECT * FROM [dbo].[EmailParsingQueue] epq WHERE [ID] = @nextJobID";

            SqlParameter agentArtifactIDParam = new SqlParameter("@agentArtifactID", System.Data.SqlDbType.Int);
            agentArtifactIDParam.Value = agentArtifactID;

            DataTable nextJobTable = eddsDBContext.ExecuteSqlStatementAsDataTable(nextJobSQL, new SqlParameter[] { agentArtifactIDParam });

            if (nextJobTable.Rows.Count > 0)
            {
                DataRow nextJobRow = nextJobTable.Rows[0];
                retVal = new EmailParsingJobDTO();
                retVal.DocumentArtifactID = Convert.ToInt32(nextJobRow["DocumentArtifactID"]);
                retVal.WorkspaceArtifactID = Convert.ToInt32(nextJobRow["WorkspaceArtifactID"]);
                retVal.Status = (JobStatus)Convert.ToInt32(nextJobRow["Status"]);

            }
            return retVal;

        }

        public string getEmailAuthorField(IDBContext caseDBContext)
        {
            string sqlText = @"SELECT EmailAuthorField FROM [dbo].[EPRFSettings] es (NOLOCK)";

            DataTable dtEmailAuthorField = caseDBContext.ExecuteSqlStatementAsDataTable(sqlText);

            string emailAuthorField  = (from e in dtEmailAuthorField.AsEnumerable()
                                            select e.Field<string>("EmailAuthorField")).FirstOrDefault().ToString();

            return emailAuthorField;

        }

        public override string Name
        {
            get
            {
                return "Email Parsing Agent";
            }
        }
    }

    public class EmailParsingJobDTO
    {
        public Int32 DocumentArtifactID { get; set; }
        public Int32 WorkspaceArtifactID { get; set; }
        public JobStatus Status { get; set; }
        public string ErrorMessage { get; set; }
    }

    public enum JobStatus
    {
        NotStarted = 0,
        InProgress = 1,
        Completed = 2,
        Error = 3
    }

}
