﻿using kCura.MassOperationHandlers;
using kCura.Relativity.Client;
using Relativity.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
namespace MassOperation
{
    [Guid("9B60FF27-BA30-4221-ADF3-36DD3AABFC00")]
    [Description("Adds documents to the Remove Footer Queue")]

    public class RemoveFooter : MassOperationHandler
    {
        
 
       public string getTableName()
        {
            string tableName = MassActionTableName;
            return tableName;
        }

        public override kCura.EventHandler.Response ValidateSelection()
        {

            
            kCura.EventHandler.Response response = new kCura.EventHandler.Response()
            {
                Success = true,
                Message = "WARNING: This may take a while!"
            };

            
            
            return response;
        }


        public override kCura.EventHandler.Response ValidateLayout()
        {
            kCura.EventHandler.Response response = new kCura.EventHandler.Response()
            {
                
                Success = true,
                Message = "WARNING: This operation permanently edits the selected objects. Confirm that you want to proceed."
            };

            return response;
        }


        public override kCura.EventHandler.Response PreMassOperation()
        {
            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response();
            retVal.Success = true;
            retVal.Message = "Successful Pre Execute Operation method";

            IDBContext eddsDBContext = Helper.GetDBContext(-1);

            InsertQueue(eddsDBContext);

            return retVal;
        }

        
        public override kCura.EventHandler.Response DoBatch()
        {



            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response();
            retVal.Success = true;


            
            this.ChangeStatus("Batch is executing");

            
            return retVal;
        }


        public DataTable getDocuments()
        {

            int caseartifactid = Helper.GetActiveCaseID();

            string sqlText = "SELECT * FROM [EDDSResource]..[" + getTableName() + "]";
            IDBContext runCase = Helper.GetDBContext(caseartifactid);
            DataTable documents = runCase.ExecuteSqlStatementAsDataTable(sqlText);

            return documents;
        }


        public void InsertQueue(Relativity.API.IDBContext eddsDBContext)
        {

            DataTable documents = getDocuments();
            
            foreach (DataRow drow in documents.Rows)
            {
                String sqlText = @"
                            INSERT INTO [dbo].[RemoveFooterQueue]
                                    ( [WorkspaceArtifactID],
                                      [DocumentArtifactID],
                                      [Status]
                                    )
                            VALUES  ( {0}, -- WorkspaceArtifactID - int
                                      {1}, -- DocumentArtifactID - int
                                      N'{2}'  -- Status - nvarchar(20)
                                    )

                            ";

                
                string documentArtifactId = drow["ArtifactID"].ToString();
                string caseArtifactId = Helper.GetActiveCaseID().ToString();
                string status = "pending";
                sqlText = string.Format(sqlText, caseArtifactId, documentArtifactId, status);

                eddsDBContext.BeginTransaction();
                try
                {
                    eddsDBContext.ExecuteNonQuerySQLStatement(sqlText);
                    eddsDBContext.CommitTransaction();

                }
                catch
                {
                    eddsDBContext.RollbackTransaction();
                    throw;
                }
            }

            

        }



        public override kCura.EventHandler.Response PostMassOperation()
        {

            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response();
            retVal.Success = true;
            retVal.Message = "Successful Post Execute Operation method";

            return retVal;
        }
        
    }
}
