﻿using kCura.MassOperationHandlers;
using kCura.Relativity.Client;
using Relativity.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MassOperation
{
    [Guid("840BDD4C-7DE5-47CC-97AB-CD98AFACE22B")]
    [Description("Adds documents to the Email Parsing Queue")]

    public class EmailParser : MassOperationHandler
    {



        public override kCura.EventHandler.Response ValidateSelection()
        {

            kCura.EventHandler.Response response = new kCura.EventHandler.Response()
            {
                Success = true,
                Message = "REQUIRED: You are about to submit documents to the Email Author Parser. Please ensure the Email Author field has been set. You can set the Email Author field in the settings."
            };



            return response;
        }


        public override kCura.EventHandler.Response ValidateLayout()
        {
            kCura.EventHandler.Response response = new kCura.EventHandler.Response()
            {
                Success = true,
                Message = "WARNING: This operation permanently edits the selected objects.Confirm that you want to proceed."
            };

            return response;
        }


        public override kCura.EventHandler.Response PreMassOperation()
        {
            //make sure the email author field is set

            string sqlText = @"SELECT  [DoesTableExist] = COUNT(*)
                FROM    [INFORMATION_SCHEMA].[TABLES] (NOLOCK)
                WHERE   [TABLE_NAME] = 'EPRFSettings'";

            IDBContext caseDBContext = Helper.GetDBContext(Helper.GetActiveCaseID());
            DataTable dtTableExists = caseDBContext.ExecuteSqlStatementAsDataTable(sqlText);

            int tableExists = int.Parse((from t in dtTableExists.AsEnumerable()
                               select t.Field<int>("DoesTableExist")).FirstOrDefault().ToString());
            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response();

            if (tableExists == 1)
            {
                IDBContext eddsDBContext = Helper.GetDBContext(-1);

                InsertQueue(eddsDBContext);

                retVal.Success = true;
                retVal.Message = "Successful Pre Execute Operation method";

            }
            else
            {
                retVal.Success = false;
                retVal.Message = "The Email Author field must be set in the settings before running the Email Parser.";
            }



            return retVal;
        }


        public override kCura.EventHandler.Response DoBatch()
        {

            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response();
            retVal.Success = true;
   

            this.ChangeStatus("Batch is executing");


            return retVal;
        }


        public DataTable getDocuments()
        {

            int caseartifactid = Helper.GetActiveCaseID();

            string sqlText = "SELECT * FROM [EDDSResource]..[" + getTableName() + "]";
            IDBContext runCase = Helper.GetDBContext(caseartifactid);
            DataTable documents = runCase.ExecuteSqlStatementAsDataTable(sqlText);

            return documents;
        }



        public void InsertQueue(Relativity.API.IDBContext eddsDBContext)
        {

            DataTable documents = getDocuments();

            foreach (DataRow drow in documents.Rows)
            {

                string sqlText = @"
                            INSERT INTO [dbo].[EmailParsingQueue]
                                    ( [WorkspaceArtifactID],
                                      [DocumentArtifactID],
                                      [AgentArtifactID],
                                      [Status],
                                      [ErrorMessage]
                                    )
                            VALUES  ( {0}, -- WorkspaceArtifactID - int
                                      {1}, -- DocumentArtifactID - int)
                                      0, -- AgentArtifactID - int
                                      0, -- Status - int
                                      N''  -- ErrorMessage - nvarchar(max)
                                    )              
                                           ";


                string documentArtifactId = drow["ArtifactID"].ToString();
                string caseArtifactId = Helper.GetActiveCaseID().ToString();

                sqlText = string.Format(sqlText, caseArtifactId, documentArtifactId);

                eddsDBContext.BeginTransaction();
                try
                {
                    eddsDBContext.ExecuteNonQuerySQLStatement(sqlText);
                    eddsDBContext.CommitTransaction();

                }
                catch
                {
                    eddsDBContext.RollbackTransaction();
                    throw;
                }
            }



        }

        public string getTableName()
        {
            string tableName = MassActionTableName;
            return tableName;
        }

        public override kCura.EventHandler.Response PostMassOperation()
        {

            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response();
            retVal.Success = true;
            retVal.Message = "Successful Post Execute Operation method";

            return retVal;
        }

    }


}
