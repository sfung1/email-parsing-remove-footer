﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kCura.EventHandler;
using System.Runtime.InteropServices;
using System.Data;

namespace Bigfoot_EventHandler
{
    [kCura.EventHandler.CustomAttributes.RunOnce(true)]
    [kCura.EventHandler.CustomAttributes.Description("Creates queue table for Remove Footer and Email Parser in the EDDS database if it not exists.")]
    [Guid("ECEDB243-3DB9-4154-AE94-5F92CC04C06D")]


    public class QueueTableCreation : kCura.EventHandler.PostInstallEventHandler
    {


        
        
        public override Response Execute()
        {
            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response();
            //Write message that appears on application details for installation.
            retVal.Message = "Successfully created RemoveFooterQueue and EmailParsingQueue tables in the EDDS database";
            retVal.Success = true;

            try 
            {
                        Relativity.API.IDBContext eddsDBContext = this.Helper.GetDBContext(-1);
                        CreateEDDSTablesIfNotExists(eddsDBContext);
            }
            catch (Exception ex)
            {
                //Catch an exception if it occurs and fail the install if the tables aren't created successfully.
                retVal.Success = false;
                retVal.Message = ex.Message;
            }
            return retVal;

            
            throw new NotImplementedException();


        }

        private static void CreateEDDSTablesIfNotExists(Relativity.API.IDBContext eddsDBContext)
        {
            String sql = @"IF NOT EXISTS ( SELECT  'true'
                            FROM    [INFORMATION_SCHEMA].[TABLES]
                            WHERE   [TABLE_NAME] = 'RemoveFooterQueue' ) 
                BEGIN 
                    CREATE TABLE [dbo].[RemoveFooterQueue]
                        (
                          [ID] INT NOT NULL
                                   IDENTITY(1, 1),
                          [WorkspaceArtifactID] INT NOT NULL,
                          [DocumentArtifactID] INT NOT NULL,
                          [Status] INT NOT NULL
                        )
                END

                IF NOT EXISTS ( SELECT  'true'
                                FROM    [INFORMATION_SCHEMA].[TABLES]
                                WHERE   [TABLE_NAME] = 'EmailParsingQueue' ) 
                    BEGIN 
                        CREATE TABLE [dbo].[EmailParsingQueue]
                            (
                              [ID] [int] NOT NULL
                                         IDENTITY(1, 1),
                              [WorkspaceArtifactID] [int] NULL,
                              [DocumentArtifactID] [int] NULL,
                              [EmailFieldName] [nvarchar](255),
                              [AgentArtifactID] [int] NULL,
                              [Status] [int] NULL,
                              [ErrorMessage] [nvarchar](MAX)
                                COLLATE SQL_Latin1_General_CP1_CI_AS
                                NULL
                            )
                        ON  [PRIMARY] TEXTIMAGE_ON [PRIMARY]

                        ALTER TABLE [dbo].[EmailParsingQueue] ADD CONSTRAINT [PK__EmailPar__3214EC270D6FE0E5] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]


                    END";

            eddsDBContext.BeginTransaction();


            try
            {
                eddsDBContext.ExecuteNonQuerySQLStatement(sql);
                eddsDBContext.CommitTransaction();
            }
            catch
            {
                eddsDBContext.RollbackTransaction();
                throw;
            }
        }

    }
}
