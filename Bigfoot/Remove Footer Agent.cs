﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Relativity.API;
using kCura.Relativity.Client;

namespace Bigfoot
{
    [kCura.Agent.CustomAttributes.Name("Bigfoot Agent")]
    [System.Runtime.InteropServices.Guid("8C8115DA-7CB0-4382-8D0F-592DD528143E")]

    
    class BigfootAgent : kCura.Agent.AgentBase
    {
              
        public DataTable GetQueue()
        {
            string batchNumber = "500";

            string sqlText = @"SELECT TOP {0}  [WorkspaceArtifactID], [DocumentArtifactID]
                                FROM    [dbo].[RemoveFooterQueue] rfq
                                WHERE   [WorkspaceArtifactID] IN ( SELECT TOP 1
                                                                            [WorkspaceArtifactID]
                                                                   FROM     [dbo].[RemoveFooterQueue] rfq
                                                                   WHERE    [Status] = 'pending'
                                                                   GROUP BY [WorkspaceArtifactID] ) AND
                                        [rfq].[Status] = 'pending'                                   ";

            sqlText = string.Format(sqlText, batchNumber);

            IDBContext runCase = Helper.GetDBContext(-1);
            DataTable returnQueue = runCase.ExecuteSqlStatementAsDataTable(sqlText);

            return returnQueue;
        }


        public DataTable GetFooterDt()
        {
            DataTable queueList = GetQueue();

            int queueCase = int.Parse((from m in queueList.AsEnumerable()
                                select m.Field<int>("WorkspaceArtifactID")).FirstOrDefault().ToString());

            if (queueCase > 0)
            {

                string sqlText = "SELECT FooterText FROM [EDDSDBO].[IdentifiedFooters] [if] (NOLOCK) WHERE FooterText IS NOT NULL";

                IDBContext runCase = Helper.GetDBContext(queueCase);
                DataTable returnFooter = runCase.ExecuteSqlStatementAsDataTable(sqlText);
                return returnFooter;

            }
            else
            { return null; }
            
            
        }

        public DataTable getExtractedTextDt()
        {
            DataTable queueList = GetQueue();

            int queueCase = int.Parse((from m in queueList.AsEnumerable()
                                       select m.Field<int>("WorkspaceArtifactID")).FirstOrDefault().ToString());

            if (queueCase > 0)
            {
                string docs = string.Join(",", queueList.AsEnumerable().Select(r => r.Field<int>("DocumentArtifactID")).ToArray());


                string sqlText = @"SELECT [ArtifactID],[ExtractedText] FROM [EDDSDBO].[Document] d (NOLOCK)
                                WHERE [ArtifactID] IN ({0})";

                sqlText = string.Format(sqlText, docs);

                IDBContext runCase = Helper.GetDBContext(queueCase);
                DataTable returnExtractedText = runCase.ExecuteSqlStatementAsDataTable(sqlText);

            }

            return returnExtractedText;
        }

        //remove footers from all domains to all documents
        public void runJob()
        {
            DataTable queueList = GetQueue();

            int queueCase = int.Parse((from m in queueList.AsEnumerable()
                                       select m.Field<int>("WorkspaceArtifactID")).FirstOrDefault().ToString());


            if (queueCase > 0)
            {

                DataTable extractedTextDt = getExtractedTextDt();
                DataTable footerDt = GetFooterDt();

                foreach (DataRow etrow in extractedTextDt.Rows)
                {

                    foreach (DataRow frow in footerDt.Rows)
                    {
                        etrow["ExtractedText"] = etrow["ExtractedText"].ToString().Replace(frow["FooterText"].ToString(), "");
                    }

                }

                foreach (DataRow updaterow in extractedTextDt.Rows)
                {
                    string extractedText = updaterow["ExtractedText"].ToString();
                    extractedText = extractedText.Replace("'", "''");
                    string documentArtifactId = updaterow["ArtifactID"].ToString();

                    string updateQuery = "UPDATE [EDDSDBO].[Document] SET [ExtractedText] = '{0}' WHERE [ArtifactID] = {1}";
                    updateQuery = string.Format(updateQuery, extractedText, documentArtifactId);

                    IDBContext updateExtractedText = Helper.GetDBContext(queueCase);
                    updateExtractedText.ExecuteNonQuerySQLStatement(updateQuery);

                    string updateQueue = "UPDATE [dbo].[RemoveFooterQueue] SET [Status] = 'complete' WHERE [DocumentArtifactID] = {0}";
                    updateQueue = string.Format(updateQueue, documentArtifactId);

                    IDBContext updateQueueTable = Helper.GetDBContext(-1);
                    updateQueueTable.ExecuteNonQuerySQLStatement(updateQueue);

                }

            }

            
        }
        
        public override void Execute()
        {

            DataTable queueList = GetQueue();

            string queueCase = (from m in queueList.AsEnumerable()
                                         select m.Field<int>("WorkspaceArtifactID")).FirstOrDefault().ToString();

            DataTable extractedTextDt = getExtractedTextDt();


            //start job
            if (extractedTextDt.Rows.Count > 0)
            {
                string recordCount = extractedTextDt.Rows.Count.ToString();
                string startMessage = "Starting Job on WorkspaceID: {0} for {1} records.";
                startMessage = string.Format(startMessage, queueCase, recordCount);
                RaiseMessage(startMessage, 1);

                runJob();
            }


            RaiseMessage("Completed", 1);


        }

        //name of the agent
        public override string Name
        {
            get
            {
                return "Remove Footer Agent";
            }
        }



    }
}
